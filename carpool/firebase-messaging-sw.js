// importScripts('https://www.gstatic.com/firebasejs/5.4.2/firebase-app.js');
// importScripts('https://www.gstatic.com/firebasejs/5.4.2/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');
console.log('getting me messaging');
var config = {
  messagingSenderId: '20640870709'
};
try {
  if ('serviceWorker' in navigator) {
    firebase.initializeApp(config);
    const messaging = firebase.messaging();

    messaging.setBackgroundMessageHandler(function(payload) {
      console.log(
        '[cloud-messaging-sw.js] Received background message ',
        payload
      );
      // Customize notification here
      var notificationTitle = 'Background Message Title';
      var notificationOptions = {
        body: 'Background Message body.',
        icon: '/firebase-logo.png'
      };

      return self.registration.showNotification(
        notificationTitle,
        notificationOptions
      );
    });
  } else {
    console.log('Service worker Push notification is not supported');
  }
} catch (error) {
  console.log('Push notification is not supported', error);
}
