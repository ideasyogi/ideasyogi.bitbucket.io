//Asset-kit extra JS code.

$(document).ready(function(){
    // Brand Design change - Mobile Hanburger click - Use links pop-up/ modal
    $('#mobile-menu').click(function(){
        $('#mobile-navigation').modal();
        $('.modal-backdrop').hide();
    });
    
    initialicePopovers();
    
    // Pop-over keypress hide/ show = Accessibility
    $('.user-li').keypress(function(){
      $('.user-popover').popover('toggle');
    });
    // Hide pop-over
    var hidepopup = function(){$(".user-popover").popover('hide');};
    $(".user-li").focusout(hidepopup); // will hide the popover    

  //Closes aby popover when clicking outside of it
  $(document).on('click', function (e) {
        $('[data-toggle="popover"],[data-original-title]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
            }

        });
    });
	
	$('.js-favstar .fa').click(function() {
		$(this).attr('class', function(i, className) {
			return className == 'fa fa-star' ? 'fa fa-star-o' : 'fa fa-star';
		 });
	});


    $('.js-calender').datepicker({
        language: "en-US",
        orientation: "auto left",
        autoclose: true,
        todayHighlight: true,
        toggleActive: true,
        daysOfWeekHighlighted: "0,6",
		daysOfWeekDisabled: "1",
        templates:{
            leftArrow: '<i class="fa fa-caret-left color-orange" aria-hidden="true"></i>', 
            rightArrow: '<i class="fa fa-caret-right color-orange" aria-hidden="true"></i>'
        }
    });
    
    
    

});

//Initialize popovers
function initialicePopovers() {
  $(".user-popover").popover({
    'placement': 'bottom',
    'html': true,
    'content': function () {
      return $('#html-popover-holder').html();
    }
  });

}

