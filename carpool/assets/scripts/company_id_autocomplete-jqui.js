$( window ).resize( function(){
    getAutoSearch();
});

$(document).ready(function(){
    getAutoSearch();
});

function getAutoSearch(){
    var getWidth = $( window ).width();    

    // WORKING JSON/ AJAX START with JQ UI
    // DDS: The "NoResultsLabel" should only be displayed if the company is not listed!
    
    var NoResultsLabel = 'My Company is not listed';
   
    $("#compNameID").autocomplete({
        delay: 100,
        minLength: 3,
        appendTo: $('#compNameID').parent(), // The PARENT of INPUT
        source: function(request, response) {
            $.getJSON("scripts/data.json", { // Local URL for testing/ Comment this and uncomment below for actual API
            //$.getJSON("https://dtl.mastercardconnect.com/currentsprint/public/portalplatform/json/MemberLookUp?classification=MER", {
                memberName: request.term
            }, function(data) {
                var array = data.error ? [] : data;
                
                if (data.length == 0) {
                    array.push({
                        Id: 0,
                        label: NoResultsLabel
                    });
                    response(array);                    
                } 
                else {
                    // data is an array of objects and must be transformed for autocomplete to use
                        array = $.map(data, function(dataItems) {
                        return {
                            label: dataItems.legalName,
                            memberId: dataItems.memberId,
                            cityName: dataItems.orgAddress[0].cityName, 
                            countryName: dataItems.orgAddress[0].countryName,
                            streetAddressOne: dataItems.orgAddress[0].streetAddressOne, 
                            countryCode: dataItems.orgAddress[0].countryCode, 
                            postCode: dataItems.orgAddress[0].postCode
                        };
                    });
                    response(array);
                    //console.log("array="+array);
                }
            });
        },
        focus: function(event, ui) {
            $('#outputbox').empty();
            $('#btnComplete').addClass('disabled').prop('disabled', true);
            $('#companyName, #notMyCompany, #addMyCompany').hide();            
            $('#chkCompanyVal').val('1');
        },
        select: function(event, ui) {            
            if(ui.item.Id === 0){		
				$('#addMyCompany').show();
				$('#btnComplete').addClass('disabled').prop('disabled', true);
				$('#chkCompanyVal').val('0');
				$('#outputbox').empty();
			}else{
                var temphtml = '';
				temphtml += '<ul>';
				temphtml += '<li class="bold">' + ui.item.value + '</li>';
				temphtml += '<li>' + ui.item.streetAddressOne + '</li>';
				temphtml += '<li>' + ui.item.cityName + ', ' + ui.item.countryCode + ' ' + ui.item.postCode +'</li>';
				temphtml += '<li>' + ui.item.countryName + '</li>';
				temphtml += '</ul>';
				$('#outputbox').html(temphtml);
				$('#companyName, #notMyCompany, #addMyCompany, .suggestedPanel').hide();
				$('#btnComplete').removeClass('disabled').removeAttr('disabled');				
				$('#chkCompanyVal').val('1');
			}
        },
    }).data("ui-autocomplete")._renderItem = function(ul, item) {
        var $p = $("<p></p>");
        if(item.Id === 0){
            // Show - NoResultsLabel = My Company is not listed.
            $("<div class='orangeText'></div>").text(item.label).appendTo($p);   
        }else{
            // Arrange the results in TWO lines!
            $("<div class='m-label bold'></div>").text(item.label + " ( " + item.memberId  + " ) ").appendTo($p);   
            $("<div class='m-location'></div>").text(item.cityName + ", " + item.countryName).appendTo($p);
        }
        return $("<li></li>").append($p).appendTo(ul);
    };
    
};