$(document).ready(function () {
	//console.log('custom.js is loaded');
	
	// Let the Tabs be loaded!
	setTimeout(function () {		
    	if ($(".select-tab").length > 0) {
			TabResizeDropdown();
		}
	}, 100);			
	
	
    // Get the HTML tag and set these attributes to tacle the IE specific issues.
    //var b = document.documentElement; 
    //b.setAttribute('data-useragent',  navigator.userAgent);
    //b.setAttribute('data-platform', navigator.platform );
    
    // Add TAB FOCUS to CHECKBOX/ RADIO
  /*  $('input[type=checkbox], input[type=radio]').on("focus", function(){
      var input = $(this);
      // assuming label is the siblings LABEL, i.e. <input /> <label>...</label>
      var label = $(this).siblings("label");
      label.css({"outline": "thin dotted #8f8f8f", "outline-offset": "3px", "text-decoration": "none", "min-height": "20px", "max-width": "inherit"});
      input.on("blur", function(){
        label.css('outline','0');
        input.off("blur");
      });
    });*/

    // IE Placehold plugin initiate!
    $('input, textarea').placeholder({
        customClass: 'custom-placeholder'
    });

    // Custom SELECT Style

    /*$(".custom-select").each(function () {
        $(this).wrap("<span class='select-wrapper'></span>");
        $(this).after("<span class='holder'></span>");
    });
	
    $(".custom-select").change(function () {
        var selectedOption = $(this).find(":selected").text();
        $(this).next(".holder").text(selectedOption);
    }).trigger('change');

    $(".custom-select").focus(function() {
        $(this).parent().css({"border-color": "#8f8f8f", "text-decoration": "none"});
        $(this).parent().css({"outline": "thin dotted #8f8f8f", "outline-offset": "3px", "text-decoration": "none"});
    });
    
    $(".custom-select").blur(function() {
      $(this).parent().css('border-color','#ccc');
      $(this).parent().css('outline','0');
    });*/
    
    
    
    // Disable the Step Tabs from being clicked.    

    $('.js-tabs').click(function (event) {
        // commented for testing!
        return false;
    });

    // Manage the Tabs correctly.
    resizeTab();
    $('#securID-tabs li .desktop-tab-label').click(function () {
        var getWidth = $(window).width();
        $('.suggestedPanel').show();
        if (getWidth < 481) {
            $('#securID-tabs').addClass('mobile-tabs-list');
            $('.mobile-tab-panel').show();
        } else {
            $('#securID-tabs').removeClass('mobile-tabs-list');
            $('.mobile-tab-panel').hide();
        }
    });

    // The TREE Manager
    $(function () {
        $('#js-tree li').hide(); // Hide the LI
        $('.ica, .blank').show(); // Show only ICA and Company.
        // Expand - Collapse the tree
        $('#js-tree li').on('click', function (e) {
            var children = $(this).find('ul > li');
            if (children.is(":visible")) {
                children.hide();
            } else {
                children.show();
            }
            e.stopPropagation();
        });
    });		

});

$(window).resize(function () {
    // Manage the Tabs correctly on resize
    resizeTab();
	
	//Tab dropdown code
	if ($(".select-tab").length > 0) {
 		removeTabAttr();
    }
	
});

function resizeTab() {
	
    // Manage the Tabs.
    var getDeviceWidth = $(window).width();
    if (/Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $(".js-tabs").each(function () {
            $('.js-tabs').removeClass('max-tablet');
            resizeTabList();
        });
    } else if (/iPad|iPod/i.test(navigator.userAgent)) {
        if (getDeviceWidth < 1024 && getDeviceWidth > 768) {
            $('.js-tabs').addClass('max-tablet');
            resizeTabList();
        } else {
            $('.js-tabs').removeClass('max-tablet');
            $(".js-tabs li").css({
                width: 'inherit'
            });
        }
    } else if (getDeviceWidth < 767) {
        $('.js-tabs').addClass('max-tablet');
        resizeTabList();
    } else {
        $('.js-tabs').removeClass('max-tablet');
        $(".js-tabs li").css({
            width: 'inherit'
        });
    }
}


function resizeTabList() {	
    // Manage the Tabs correctly.
    var getLength = 0,
        getUlWidth = 0,
        evenWidth = 0,
        setListMargin = 10,
        getTabList;
    $(".js-tabs").each(function () {		
        if ($(".js-tabs li").hasClass('hide')) {
            getTabList = $(".js-tabs li:not('.hide')");
        } else {
            getTabList = $(".js-tabs li");
        }
        getLength = getTabList.length - 1;
        getUlWidth = $('.js-tabs').width() - (getLength * setListMargin);
        evenWidth = getUlWidth / getTabList.size();
        getTabList.css("width", evenWidth);
    });
}


function formEmptyValidation(id, formid) {
    var valReturn = true;
    $('#' + formid).find('span[id^="errorSpan"]').remove();
    $('#' + formid).find('span[class^="sprite icon-error"]').remove();
    $('#' + formid).removeClass("error");
    if (formid == 'mccSignIn') {
        $('#' + formid).find("input[type='text'], input[type='password']").each(function () {
            if ($(this).val().trim() == "") {
                $(this).addClass('error');
                $(this).parent().append("<span class=\"sprite icon-error\"></span>");
                $(this).parent().append("<span id='errorSpan' class='input-text-error'>This field is empty</span>");
                valReturn = 'true';
            } else {
                valReturn = 'false';
            }
        });
    } else {
        $('#' + id).find("input[type='text'],input[type='password']").each(function () {
            if ($(this).val().trim() == "") {
                $(this).addClass('error');
                $(this).parent().append("<span class=\"sprite icon-error\"></span>");
                $(this).parent().append("<span id='errorSpan' class='input-text-error'>This field is empty</span>");
                valReturn = 'true';
            } else {
                valReturn = 'false';
            }
        });
    }
    return valReturn;
}

function buttonPrevious(id) {
    if (id == 'btnPre1') {
        activaTab('tab1');
    } else if (id == 'btnPre2') {
        activaTab('tab2');
    } else if (id == 'btnPre3') {
        activaTab('tab3');
    }
    $(".form-group").find('span[id^="errorSpan"]').remove();
    $(".form-group").find('span[id^="pass"]').remove();
    $(".form-group").find('span[class^="sprite icon-error"]').remove();
}

function navigateSteps(tab) {	
    $('.step-nav span[data-target="#' + tab + '"]').tab('show');
	$('.step-nav > li.active > span').addClass('active');
}

function markStepsDone(tab) {	
    $('.step-nav span[data-target="#' + tab + '"]').addClass('done');
}

function activaTab(tab) {
    $('.nav-tabs span[data-target="#' + tab + '"]').tab('show');
}

function tabStatusDone(tab) {
    $('.nav-tabs span[data-target="#' + tab + '"]').addClass('done');
}

function signinSignUpValidation(id, constraintType) {
    $(".form-group").find('span[class^="sprite icon-error"]').remove();
    $(document).ready(function () {
        if (id == 'btnComplete') {
            location.href = "complete_signup.html";
            return false;
        }
    });
    return false;
}

function tabNavigating(id) {

    if (id == 'btnNext1') {
        activaTab('tab2');
        tabStatusDone('tab1');
    } else if (id == 'btnNext2') {
        activaTab('tab3');
        tabStatusDone('tab2');
    } else if (id == 'btnNext3') {
        activaTab('tab4');
        tabStatusDone('tab3');
    } else if (id == 'btnChnagePass') {
        $('#chkComplete').hide();
        $('#completeMsg').show();
        tabStatusDone('tab3');
    }
}

function formValidate(id, constraintType) {

    var formid = $('form').attr('id');
    var formData = $('#' + formid).serialize();
    var tabId = "";
    $('.nav-tabs li').each(function () {
        if ($(this).hasClass('active')) {
            tabId = $(this).find('span').attr('aria-controls');
        }
    });

    var errorFound = formEmptyValidation(tabId, formid);

    if (id == 'btnNext1' || id == 'btnNext2' || id == 'btnNext3' || id == 'btnChnagePass') {
        if (errorFound == 'true') {
            // return false;
        } else {
            tabNavigating(id);
            event.preventDefault();
            return false;
        }
    }

    // Sample Fullpage loading effect
    if (id == 'btnSignIn') {
        var userid = $("#userid").val();
        var password = $("#password").val();
        if (errorFound == 'true') {
            return false;
        } else {
            if (userid !== '' && password !== '') {
                $("#loading").show();
                setTimeout(function () {
                    location.href = "dashboard.html";
                }, 1000);
                return false;

            }
        }
    }

}

function removeErrors(divName, eleName) {
    if ($("#" + eleName).hasClass('error')) {
        $("#" + divName).find('span[id^="errorSpan"]').remove();
        $("#" + divName).find('span[id^="pass"]').remove();
        $("#" + divName).find('span[class^="sprite icon-error"]').remove();
        $("#" + eleName).removeClass("error");
    }
    return false;
}

function validateOnFocusOut(divName, eleName) {

    $("#" + divName).find('span[id^="pass"]').remove();
    $("#" + divName).find('span[id^="errorSpan"]').remove();
    $("#" + divName).find('span[class^="sprite icon-error"]').remove();
}

function validateOnKeyup(divName, eleName) {


}

$("form").submit(function (e) {
    return false;
});

/*$('.js-tabs').click(function(){
    $(".form-group").find('span[id^="errorSpan"]').remove();
    $(".form-group").find('span[id^="pass"]').remove();
    $(".form-group").find('span[class^="sprite icon-error"]').remove();
});*/

$('#btnAccountLockedClose').click(function () {
    $(".form-group").find('span[class^="sprite icon-error"]').remove();
});

//Tab dropdown function code starts

function removeTabAttr() {
	var deviceWidth = $(document).width();
	if (deviceWidth > 480) {
		$(".tab-nav").removeAttr("style");
	}
}

function TabResizeDropdown() {
	//console.log('tabs');
	var $selecttab = $('.select-tab');

	$selecttab.each(function(i, elm) {
		var SelectedStr = $(elm).next('ul').find('li.active span').text();
        if($(elm).next('ul').find('li.active .badge-counter').length > 0) {
            var countBadge =  $(elm).next('ul').find('li.active .badge-counter').text();
            SelectedStr = SelectedStr + '<span class="badge-counter">' + countBadge + '</span>';
        }
		$(elm).html(SelectedStr);
	});

	$('.select-tab').on('click', function(e) {
		e.preventDefault();
		$('.tab-nav').slideToggle();
	});

	$('.tab-nav li[data-target]').on('click', function(e) {
		e.preventDefault();
		$selecttab.html("");
		var SelectedStr = $(this).find('span').text(),
			deviceWidth = $(document).width(),
			countBadge = $(this).find('.badge-counter').text();
		
		if($(this).find('.badge-counter').length > 0) {
            SelectedStr = SelectedStr + '<span class="badge-counter">' + countBadge + '</span>';
        }
		
		$(e.target).closest('ul').prev('button').html(SelectedStr);
		if ((deviceWidth <= 480) ||  ($('.tabDropdowncheck').length > 0)) {
			$(e.target).closest('ul').hide();
		}
	});

	// function for make the tab as dropdown when tab list width is greater than the device width then      
	tabDropdowncheck();
}

 //Tab dropdown function code ends
 // code for check if tab list width is greater than the device width then make the tab as dropdown

var resetDeviceWidth = 0;
function tabDropdowncheck() {
    var deviceWidth = $(document).width(),
        brandTabWidth = $('.brand-tabs').width(),   
        TabMenuWidth = $('.tab-nav').width(),
        $brandTabs = $(".brand-tabs");
        if (deviceWidth > 480) {
            if(brandTabWidth == TabMenuWidth) {                
                $brandTabs.addClass("tabDropdowncheck");
            } else {
                $brandTabs.removeClass("tabDropdowncheck");
            }            
        }
        //window resize code 
        $(window).resize(function(){
			//console.log('resizgin');
            deviceWidth = $(document).width();
            brandTabWidth = $('.brand-tabs').width();   
            TabMenuWidth = $('.tab-nav').width();

            if (deviceWidth > 480) {   
                if(brandTabWidth == TabMenuWidth) {
                    resetDeviceWidth = deviceWidth;
                    $brandTabs.addClass("tabDropdowncheck");
                } else {
                    if(resetDeviceWidth < brandTabWidth)  {
                        $brandTabs.removeClass("tabDropdowncheck");
                    }
                 }
            }
        });
 }

 //Tab dropdown function code ends