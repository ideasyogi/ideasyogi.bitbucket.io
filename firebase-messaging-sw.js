importScripts('https://www.gstatic.com/firebasejs/5.4.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.4.2/firebase-messaging.js');
console.log('getting me messaging');
var config = {
  messagingSenderId: '20640870709'
};
try{
	console.log("Push notification 1");

	  console.log("Push notification 2");
      firebase.initializeApp(config);
	  console.log("Push notification 3");
      const messaging = firebase.messaging();
	  console.log("Push notification 4");

      messaging.setBackgroundMessageHandler(function(payload) {
        console.log('[cloud-messaging-sw.js] Received background message ', payload);
        // Customize notification here
        var notificationTitle = 'Background Message Title';
        var notificationOptions = {
          body: 'Background Message body.',
          icon: '/firebase-logo.png'
        };

        return self.registration.showNotification(
          notificationTitle,
          notificationOptions
        );
      });

}catch(error){
  console.log("Push notification is not supported",error);
}
